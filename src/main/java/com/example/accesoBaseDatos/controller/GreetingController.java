package com.example.accesoBaseDatos.controller;

import com.example.accesoBaseDatos.dao.Usuarios;
import com.example.accesoBaseDatos.dto.UsuariosWrapper;
import com.example.accesoBaseDatos.service.UsuariosService;
import java.util.List;
import javax.annotation.Resource;
import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @Resource
    UsuariosService userService;
    
    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
        
        // Ejemplo de uso de los datos de la BD
        List<Usuarios> usuarios = userService.getAll();
        if(name.equals("World")){
            name = usuarios.get(0).getNombre();
        }
        
        UsuariosWrapper wrapper = new UsuariosWrapper();
        wrapper.setUsers(usuarios);
        
        JSONArray list = new JSONArray(usuarios);
        
        
        model.addAttribute("list", list);
        model.addAttribute("wrapper", wrapper);
        model.addAttribute("name", name);
        return "greeting";
    }
    
    @RequestMapping("/perfom_login")
    public String login(Model model) {
        return "greeting";
    }
    
    @GetMapping("/form")
    public String formulario(Model model) {
        return "formulario";
    }
    
    @RequestMapping("/listadoUsuarios")
    public String listadoUsuarios(Model model) {
        return "listadoUsuarios";
    }

}
